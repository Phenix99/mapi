import { DateTime } from 'luxon'
import User from "App/Models/User";
import {
  column,
  BaseModel,
  hasOne,
  HasOne,
} from "@ioc:Adonis/Lucid/Orm";

export default class Modele extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public nom: string

  @column()
  public description: string

  @column()
  public image_url: string

  @column()
  public fichier_url: string

  @column()
  public schema: string

  @column()
  public uischema: string

  @hasOne(() => User)
  public user: HasOne<typeof User>;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
