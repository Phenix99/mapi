import { DateTime } from 'luxon'
import User from "App/Models/User";
import Modele from "App/Models/Modele";
import {
  column,
  BaseModel,
  hasOne,
  HasOne,
} from "@ioc:Adonis/Lucid/Orm";

export default class Avi extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public comment: string;

  @column()
  public note: number;

  @hasOne(() => User)
  public user: HasOne<typeof User>;

  @hasOne(() => Modele)
  public modele: HasOne<typeof Modele>;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
