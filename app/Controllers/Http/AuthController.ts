import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import User from "App/Models/User";

export default class AuthController {
  public async login({ request, auth }: HttpContextContract) {
    const email = request.input("email");
    const password = request.input("password");
    const token = await auth.use("api").attempt(email, password, {
      expiresIn: "10 days",
    });
    return token.toJSON();
  }
  public async register({ request, auth }: HttpContextContract) {
    /* const email = request.input("email");
    const password = request.input("password");
    const nom = request.input("nom");
    const prenom = request.input("prenom");
    const telephone = request.input("telephone"); */
    const newUser = new User();
    newUser.fill(request.all());
    /* newUser.email = email;
    newUser.password = password;
    newUser.nom = nom;
    newUser.prenom = prenom;
    newUser.telephone = telephone; */
    await newUser.save();
    const token = await auth.use("api").login(newUser, {
      expiresIn: "10 days",
    });
    return token.toJSON();
  }
}