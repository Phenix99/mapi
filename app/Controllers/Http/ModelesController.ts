import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
    import Modele from "App/Models/Modele";
    export default class ModelesController {
         public async index({ }: HttpContextContract)
        {
            const modeles = await Modele.query();
            return modeles
        }
        public async show({ params}: HttpContextContract)
        {
            try {
                const modele = await Modele.find(params.id);
                if(modele){
                    await modele.preload('user')
                    return modele
                }
            } catch (error) {
                console.log(error)
            }

        }

        public async update({ request, params}: HttpContextContract)
        {
            const modele = await Modele.find(params.id);
            if (modele) {
                modele.nom = request.input('nom');
                modele.description = request.input('description');
                if (await modele.save()) {
                    await modele.preload('user')
                    return modele
                }
                return; // 422
            }
            return; // 401
        }

        public async store({ auth, request}: HttpContextContract)
        {
            const user = await auth.authenticate();
            const modele = new Modele();
            modele.nom = request.input('nom');
            modele.description = request.input('description');
            await user.related('modeles').save(modele)
            return modele
        }
        public async destroy({response}: HttpContextContract)
        {
           return response.redirect('/dashboard');
        }
    }