import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Modeles extends BaseSchema {
  protected tableName = 'modeles'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string("nom", 255).notNullable();
      table.string("description").notNullable();
      table.string("image_url", 255).notNullable();
      table.string("fichier_url", 150).notNullable();
      table.string("schema").notNullable();
      table.string("uischema").notNullable();
      table.integer('user_id').nullable().unsigned().references('id').inTable('users')
      table.integer('categorie_id').nullable().unsigned().references('id').inTable('categories')
      table.integer("like").nullable();

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
