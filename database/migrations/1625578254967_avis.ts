import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Avis extends BaseSchema {
  protected tableName = 'avis'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("note");
      table.string("comment");
      table.integer('modele_id').notNullable().unsigned().references('id').inTable('modeles').onDelete('CASCADE')
      table.integer('user_id').notNullable().unsigned().references('id').inTable('users').onDelete('CASCADE')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
